// vdso-grindit.<ARCH>: load tester for (expected) VDSO syscalls

// Copyright © 2015 Vivek Das Mohapatra <vivek@collabora.com>, <vivek@etla.org>
// Copyright © 2015 Collabora ltd.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <sched.h>

struct timeval tv;

#define HELP_TEXT \
    "usage: %s <ITERATIONS> [SYSCALL]\n" "\n"                                \
    "  ITERATIONS: Positive integer, or 0 or -ve for the default\n"          \
    "  SYSCALL: One of t, g or c for time(), gettimeofday() or getcpu()\n"   \
    "           or f to check the obsolete ftime() call + gettimeofday.\n"   \
    "           If omitted or not recognised, gettimeofday is assumed.\n\n"  \
    "suggested usage: time %1$s 0x1000000 g\n"                               \
    "  If VDSO is working for the syscall in question the sys time will be\n"\
    "  0.000 seconds or close to it, even for large iteration values.\n\n"

int main (int argc, char **argv)
{
    long int loop = -1;
    struct timeval  tv = { 0 };
    struct timezone tz = { 0 };
    struct timeb    tb = { 0 };
    char syscall = 'g';

    if (argc < 2)
    {
        fprintf(stderr, HELP_TEXT, argv[0]);
        exit(1);
    }

    if (argc >= 2)
    {
        char *end = NULL;
        loop = strtoll( argv[1], &end, 0 );
        if( argv[1] == end )
            loop = -1;
    }

    if (argc >= 3)
    {
        switch (*argv[2])
        {
          case 'c':
          case 'f':
          case 'g':
          case 't':
            syscall = *argv[2];
            break;
          default:
            syscall = 'g';
            break;
        }
    }

    switch (syscall)
    {
      case 't':
        fprintf(stdout, "Testing time(NULL)\n");
        break;
      case 'c':
        fprintf(stdout, "Testing getcpu(NULL)\n");
        break;
      case 'f':
        fprintf(stdout, "Testing ftime() (DEPRECATED)\n");
        break;
      default:
        fprintf(stdout, "Testing gettimeofday(struct timeval *, …)\n");
    }

    if (loop <= 0)
        loop = 0x1000000;

    for (; loop >= 0; loop--)
    {
        switch (syscall)
        {
            int cpu;
            time_t now;
            time_t then;
          case 't':
            now = time(&then);
            break;
          case 'c':
            cpu = sched_getcpu();
            break;
          case 'f':
            __gettimeofday(&tv, NULL);
            ftime (&tb);
            loop--;
            break;
          default:
            gettimeofday( &tv, &tz );
        }
    }

    return 0;
}
