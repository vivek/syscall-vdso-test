// Basic gettimeofday syscall test (not v interesting except to check for crashes)

// Copyright © 2015 Vivek Das Mohapatra <vivek@collabora.com>, <vivek@etla.org>
// Copyright © 2015 Collabora ltd.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

struct timeval tv;

int main (int argc, char **argv)
{
    struct timeval  tv = { 0 };

    fprintf(stderr, "making syscalls\n");
    time_t now = time(NULL);

    gettimeofday( &tv, NULL );
    fprintf( stderr, "\n{ %ld . %lu }\n{ %ld . … }\n", tv.tv_sec, tv.tv_usec, now );

    return 0;
}
