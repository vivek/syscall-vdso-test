# Copyright © 2015 Vivek Das Mohapatra <vivek@collabora.com>, <vivek@etla.org>
# Copyright © 2015 Collabora ltd.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


.PHONY: clean all

arch := $(patsubst i%86,ix86,$(shell uname -m))
ifeq ($(arch),ix86)
lgcc_s := -lgcc_s
else
lgcc_s :=
endif

BINARIES := vdso-gettimeofday.$(arch) parse-vdso.$(arch) vdso-grindit.$(arch)
CFLAGS := $(CFLAGS) -std=gnu99

all: $(BINARIES)

clean:
	 rm -vf $(BINARIES)

%.$(arch):
	$(CC) $(CFLAGS) $^ -o $@

vdso-grindit.$(arch): vdso-grindit.c

vdso-gettimeofday.$(arch): CFLAGS += -O0 -g3
vdso-gettimeofday.$(arch): vdso-gettimeofday.c

parse-vdso.$(arch): CFLAGS += -nostdlib -Os -g3
parse-vdso.$(arch): CFLAGS += -fno-asynchronous-unwind-tables -flto $(lgcc_s)
parse-vdso.$(arch): parse_vdso.c vdso_standalone_test_x86.c
